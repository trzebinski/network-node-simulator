# Network Node Simulator

Network Node Simulator project using queuing algorithms (FIFO, HTB, PQ, SJF, WFQ) and access control algorithms (RED, RIO, WRED) to manage packages transfers.

Technologies
---
Project is created with: Java, Swing

Getting Started
---
In home directory, run the following command to download project:

    git clone https://trzebinski@bitbucket.org/trzebinski/network-node-simulator.git

In downloaded directory, run the following command:

    java -jar NetworkNodeSimulator.jar

Screenshot examples
---
![SCREENSHOT](https://i.ibb.co/2P1m1s9/SCREENSHOT.png)

![SCREENSHOT](https://i.ibb.co/cwrQYQx/SCREENSHOT.png)