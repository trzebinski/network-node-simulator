package gui;

import gui.charts.AccessBarChartPanel;
import gui.charts.AccessLineChartPanel;
import gui.charts.AccessMultiBarChartPanel;
import jdistlib.generic.GenericDistribution;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GUI {
    private ChooseDistributionsPanel chooseDistributionsPanel;
    private JFrame frame;
    private JLabel label;
    public static AccessBarChartPanel ACCESS_BAR_CHART_PANEL;
    public static AccessLineChartPanel ACCESS_LINE_CHART_PANEL;
    public static AccessMultiBarChartPanel ACCESS_MULTI_BAR_CHART_PANEL;
    public static GraphPanel graphPanel;

    public GUI() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame("Symulator węzła sieciowego");
        frame.setBounds(100, 100, 920, 600);
        frame.setMinimumSize(new Dimension(800, 600));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout(0, 0));

        JLabel GeneratorsLabel = new JLabel("Generatory");
        frame.getContentPane().add(GeneratorsLabel, BorderLayout.NORTH);

        chooseDistributionsPanel = new ChooseDistributionsPanel();
        frame.getContentPane().add(chooseDistributionsPanel, BorderLayout.NORTH);

        AlgorithmPanel algorithmPanel = new AlgorithmPanel(this);
        frame.getContentPane().add(algorithmPanel, BorderLayout.WEST);

        ButtonPanel buttonPanel = new ButtonPanel(this, algorithmPanel);
        frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

        graphPanel = new GraphPanel();
        ACCESS_BAR_CHART_PANEL = new AccessBarChartPanel("Liczba pakietów przyjętych i odrzuconych");
        ACCESS_MULTI_BAR_CHART_PANEL = new AccessMultiBarChartPanel("Procent pakietów przyjętych " +
                "i odrzuconych w zależności od priorytetu");
        ACCESS_LINE_CHART_PANEL = new AccessLineChartPanel("Procent pakietów przyjętych " +
                "i odrzuconych w czasie");
        tabbedPane.add("Zapełnienie kolejki", graphPanel);
        tabbedPane.add("Odrzucenia ze względu na priorytety", ACCESS_MULTI_BAR_CHART_PANEL);
        tabbedPane.add("Odrzucenia", ACCESS_BAR_CHART_PANEL);
        tabbedPane.add("Odrzucenia w czasie", ACCESS_LINE_CHART_PANEL);
        frame.getContentPane().add(tabbedPane, BorderLayout.EAST);
        frame.setVisible(true);
    }

    public JLabel getLabel() {
        return label;
    }

    public ArrayList<GenericDistribution> getChooseDistributionsPanel() {
        return chooseDistributionsPanel.getDistributions();
    }

    public ArrayList<Integer> getCountOfEachTrafficType() {
        ArrayList<Integer> toReturn = new ArrayList<>();
        for (JSpinner s : chooseDistributionsPanel.getCountOfEachTraffic()) {
            toReturn.add((Integer) s.getValue());
        }
        return toReturn;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    JFrame getFrame() {
        return frame;
    }
}
