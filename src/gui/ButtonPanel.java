package gui;

import core.Main;
import core.Simulation;
import core.Switch;
import gui.charts.HistogramFrame;
import gui.charts.MultiHistogramFrame;
import gui.charts.WaitingTimeFrame;
import statistics.InputData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

class ButtonPanel extends JPanel {
    private static JButton STOP;
    private AlgorithmPanel algorithmPanel;
    private GUI gui;
    private JCheckBox isStopAfterTimeCheckBox;
    private JSpinner spinner;
    private Thread threadOfStopActionAfterSpecifiedTime = null;
    private boolean isStarted = false;
    private boolean stopByThread = false;
    private boolean wasStartedOnce = false;

    ButtonPanel(GUI gui, AlgorithmPanel algorithmPanel) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);

        this.gui = gui;
        this.algorithmPanel = algorithmPanel;
        int marginUpDown = 10;

        JButton START = new JButton("START");
        GridBagConstraints start = new GridBagConstraints();
        start.anchor = GridBagConstraints.CENTER;
        start.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        start.gridx = 0;
        start.gridy = 0;
        START.addActionListener(getStartActionListener());
        this.add(START, start);

        STOP = new JButton("STOP");
        GridBagConstraints stop = new GridBagConstraints();
        stop.anchor = GridBagConstraints.CENTER;
        stop.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        stop.gridx = 1;
        stop.gridy = 0;
        STOP.addActionListener(getStopActionListener());
        this.add(STOP, stop);

        isStopAfterTimeCheckBox = new JCheckBox("Czy zatrzymać po:");
        GridBagConstraints stopAfterTimeCheckBox = new GridBagConstraints();
        stopAfterTimeCheckBox.anchor = GridBagConstraints.CENTER;
        stopAfterTimeCheckBox.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        stopAfterTimeCheckBox.gridx = 2;
        stopAfterTimeCheckBox.gridy = 0;
        add(isStopAfterTimeCheckBox, stopAfterTimeCheckBox);

        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(3, 0, 9999, 1);
        spinner = new JSpinner(spinnerNumberModel);
        ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setColumns(2);
        GridBagConstraints spinner = new GridBagConstraints();
        spinner.anchor = GridBagConstraints.CENTER;
        spinner.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        spinner.gridx = 3;
        spinner.gridy = 0;
        add(this.spinner, spinner);

        JLabel timeLabel = new JLabel("sekundach");
        GridBagConstraints time = new GridBagConstraints();
        time.anchor = GridBagConstraints.CENTER;
        time.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        time.gridx = 4;
        time.gridy = 0;
        add(timeLabel, time);

        JButton histogramButton = new JButton("Zapisz histogramy");
        GridBagConstraints histogram = new GridBagConstraints();
        histogram.anchor = GridBagConstraints.CENTER;
        histogram.insets = new Insets(marginUpDown, 0, marginUpDown, 5);
        histogram.gridx = 5;
        histogram.gridy = 0;
        histogramButton.addActionListener(getHistogramActionActionListener());
        this.add(histogramButton, histogram);
    }

    private ActionListener getHistogramActionActionListener() {
        return e -> {
            if (!wasStartedOnce) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Nie uruchomiono symulacji",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (isStarted) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Historgramy zostały już zapisane",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
            } else {
                isStarted = true;

                Switch simulationSwitch = Main.simulation.getSwitch();
                List<Long> requestsTimes = simulationSwitch.getTimeOfAllArrivalPackets();
                List<Long> allowRequestsTimes = simulationSwitch.getTimeOfAllowArrivalPackets();
                final long timeOfFirstArrive = requestsTimes.get(0);
                double[] rerestsTimeDouble = prepareRelativeTimeArray(requestsTimes, timeOfFirstArrive);
                double[] allowRerestsTimeDouble = prepareRelativeTimeArray(allowRequestsTimes, timeOfFirstArrive);
                List<List<Long>> requestsTimesByPriority = simulationSwitch.getTimeOfAllArrivalPacketsByPriority();
                List<List<Long>> allowRequestsTimesByPriority =
                        simulationSwitch.getTimeOfAllowArrivalPacketsByPriority();
                List<double[]> requestsTimesByPriorityDouble = new ArrayList<>();
                List<double[]> allowRequestsTimesByPriorityDouble = new ArrayList<>();

                for (int i = 0; i < Switch.PRIORITY_CLASS_COUNT; i++) {
                    double[] reqTimeByPriority = prepareRelativeTimeArray(
                            requestsTimesByPriority.get(i), timeOfFirstArrive);
                    double[] reqAllowTimeByPriority = prepareRelativeTimeArray(
                            allowRequestsTimesByPriority.get(i), timeOfFirstArrive);
                    requestsTimesByPriorityDouble.add(reqTimeByPriority);
                    allowRequestsTimesByPriorityDouble.add(reqAllowTimeByPriority);
                }

                new Thread(() -> {
                    GUI.ACCESS_BAR_CHART_PANEL.saveAsImg();
                    GUI.ACCESS_LINE_CHART_PANEL.saveAsImg();
                    GUI.ACCESS_MULTI_BAR_CHART_PANEL.saveAsImg();
                    GUI.graphPanel.saveAsImg();
                }).start();

                new HistogramFrame(rerestsTimeDouble, allowRerestsTimeDouble);
                new MultiHistogramFrame(requestsTimesByPriorityDouble, allowRequestsTimesByPriorityDouble);
                new WaitingTimeFrame();
            }
        };
    }

    private ActionListener getStartActionListener() {
        return e -> {
            if (isStarted) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Symulacja została już rozpoczęta",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
                return;
            }

            wasStartedOnce = true;
            boolean allGeneratorsCorrect = true;
            for (int i = 0; i < 5; i++) {
                int selectedClassCount = gui.getCountOfEachTrafficType().get(i);

                if (selectedClassCount < 0)
                    allGeneratorsCorrect = false;
                Simulation.classCount[i] = selectedClassCount;
            }

            if (!allGeneratorsCorrect) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Wybrano za mało generatorów",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
            } else {
                isStarted = true;

                InputData inputData = new InputData(gui);
                inputData.allData();
                GUI.graphPanel.clearDataSet();
                GUI.ACCESS_LINE_CHART_PANEL.clearDataSet();

                Main.simulation = new Simulation(gui, algorithmPanel.getChosenQueuingAlgorithm(),
                        algorithmPanel.getChosenAccessControlAlgorithm());
                if (isStopAfterTimeCheckBox.isSelected()) {
                    threadOfStopActionAfterSpecifiedTime = getThreadOfStopActionAfterSpecifiedTime();
                    threadOfStopActionAfterSpecifiedTime.start();
                }
            }
        };
    }

    private ActionListener getStopActionListener() {
        return e -> {
            if (!wasStartedOnce) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Nie uruchomiono symulacji",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!isStarted) {
                JOptionPane.showMessageDialog(gui.getFrame(), "Symulacja została już zakończona",
                        "Błąd", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (threadOfStopActionAfterSpecifiedTime != null && stopByThread) {
                threadOfStopActionAfterSpecifiedTime.interrupt();
            }

            Main.simulation.interuptAll();

            InputData inputData = new InputData(gui);
            inputData.readData();
            InputData.WHETHER_PARAMETERS_LOADED = false;
            List<Long> requestsTimes = Main.simulation.getSwitch().getTimeOfAllArrivalPackets();
            saveHistoryOfComingAllRequestsToFile(requestsTimes);
            isStarted = false;
            threadOfStopActionAfterSpecifiedTime = null;
        };
    }

    private Thread getThreadOfStopActionAfterSpecifiedTime() {
        return new Thread(() -> {
            try {
                Thread.sleep((int) spinner.getValue() * 1000);
                stopByThread = true;
                STOP.doClick();
                stopByThread = false;
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        });
    }

    private double[] prepareRelativeTimeArray(List<Long> times, long startTime) {
        ToDoubleFunction<Long> changeTimeOfPackagesToSeconds = rawTime -> (rawTime.doubleValue() - startTime) / 10e8;
        return times.stream().mapToDouble(changeTimeOfPackagesToSeconds).toArray();
    }

    private void saveHistoryOfComingAllRequestsToFile(List<Long> requestsTimes) {
        new Thread(() -> {
            if (requestsTimes.isEmpty())
                return;

            final long TIME_OF_FIRST_ARRIVE = requestsTimes.get(0);
            Function<Long, String> getStringOfArrivalTime = (req) -> Long.toString(req - TIME_OF_FIRST_ARRIVE);
            Stream<String> reqestsTimeStream = requestsTimes.stream().map(getStringOfArrivalTime);
            try {
                Files.write(Paths.get("results\\" + "allRequests.csv"),
                        (Iterable<String>) reqestsTimeStream::iterator);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
