package gui;

import jdistlib.generic.GenericDistribution;
import statistics.InputData;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

class ChooseDistributionsPanel extends JPanel {
    private ArrayList<DistributionSetterPanel> distributionSetterPanel;
    private ArrayList<JSpinner> countOfEachTraffic;

    ChooseDistributionsPanel() {
        countOfEachTraffic = new ArrayList<>();
        distributionSetterPanel = new ArrayList<>();

        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);

        JLabel numberLabel = new JLabel("Liczba");
        GridBagConstraints number = new GridBagConstraints();
        number.anchor = GridBagConstraints.WEST;
        number.insets = new Insets(0, 0, 5, 5);
        number.gridx = 0;
        number.gridy = 0;
        this.add(numberLabel, number);

        JLabel generatorLabel = new JLabel("Generatory");
        GridBagConstraints generator = new GridBagConstraints();
        generator.anchor = GridBagConstraints.CENTER;
        generator.insets = new Insets(0, 0, 5, 5);
        generator.gridx = 1;
        generator.gridy = 0;
        this.add(generatorLabel, generator);

        int numberOfDistributions = 3;
        for (int i = 0; i < numberOfDistributions; i++) {
            String[] namesOfDistributionDestinations = {"Alfa", "Beta", "Gamma"};
            String[] namesOfMeaningDestinations = {"Rozmiar Paczki", "Odstępy czasu pomiędzy pakietami",
                    "Odstępy czasu pomiędzy paczkami"};

            JLabel parameterLabel = new JLabel(namesOfDistributionDestinations[i]);
            parameterLabel.setToolTipText(namesOfMeaningDestinations[i]);

            GridBagConstraints parameter = new GridBagConstraints();
            parameter.anchor = GridBagConstraints.CENTER;
            parameter.insets = new Insets(0, 0, 5, 5);
            parameter.gridx = 2 + i;
            parameter.gridy = 0;
            this.add(parameterLabel, parameter);
        }

        int countOfTraficType = 5;
        for (int i = 0; i < countOfTraficType; i++) {
            SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(1, 0, 9999, 1);
            JSpinner jSpinner = new JSpinner(spinnerNumberModel);
            ((JSpinner.DefaultEditor) jSpinner.getEditor()).getTextField().setColumns(2);
            GridBagConstraints spinner = new GridBagConstraints();
            spinner.gridx = 0;
            spinner.gridy = i + 1;
            add(jSpinner, spinner);
            countOfEachTraffic.add(jSpinner);

            JLabel classOfTrafficLabel = new JLabel(" x Klasa ruchu nr " + (i + 1) + ":");
            GridBagConstraints classOfTraffic = new GridBagConstraints();
            classOfTraffic.anchor = GridBagConstraints.WEST;
            classOfTraffic.insets = new Insets(0, 0, 5, 5);
            classOfTraffic.gridx = 1;
            classOfTraffic.gridy = i + 1;
            this.add(classOfTrafficLabel, classOfTraffic);

            for (int j = 0; j < numberOfDistributions; j++) {
                DistributionSetterPanel distributionSetterPanel = new DistributionSetterPanel();
                GridBagConstraints distributionSetter = new GridBagConstraints();
                distributionSetter.anchor = GridBagConstraints.CENTER;
                distributionSetter.insets = new Insets(0, 0, 5, 5);
                distributionSetter.gridx = 2 + j;
                distributionSetter.gridy = i + 1;
                this.add(distributionSetterPanel, distributionSetter);
                this.distributionSetterPanel.add(distributionSetterPanel);
            }
        }
        sendSpinners();
    }

    private void sendSpinners() {
        InputData.SPINNER_LISTS.addAll(countOfEachTraffic);
    }

    ArrayList<GenericDistribution> getDistributions() {
        ArrayList<GenericDistribution> genericDistribution = new ArrayList<>();
        for (DistributionSetterPanel distributionSetterPanel : distributionSetterPanel) {
            genericDistribution.add(distributionSetterPanel.getDistributionGenerator());
        }
        return genericDistribution;
    }

    ArrayList<JSpinner> getCountOfEachTraffic() {
        return countOfEachTraffic;
    }
}
