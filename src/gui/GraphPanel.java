package gui;

import gui.charts.HistogramSaver;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;

public class GraphPanel extends JPanel {
    private static String CHART_TITLE = "Wykorzystanie przepustowości węzła w czasie";
    private static int TIME_TO_SHOW = Integer.MAX_VALUE;
    private XYSeries throughputUsing;
    private XYSeries filteredThroughputUsing;
    private XYSeriesCollection dataSet;
    private JButton button;
    private JCheckBox checkBox;
    private JFreeChart lineChart;
    private JSpinner spinner;

    GraphPanel() {
        this.dataSet = new XYSeriesCollection();
        this.throughputUsing = new XYSeries("Zapełnienie węzła");
        this.filteredThroughputUsing = new XYSeries("zapełnienie węzła");

        dataSet.addSeries(filteredThroughputUsing);

        lineChart = ChartFactory.createXYLineChart(
                CHART_TITLE,
                "czas",
                "procent zapełnienia",
                dataSet,
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new Dimension(500, 300));
        add(chartPanel);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setPreferredSize(new Dimension(100, 100));
        JPanel panelA = new JPanel();
        panelA.setAlignmentX(LEFT_ALIGNMENT);

        checkBox = new JCheckBox("Wybierz okres");
        panelA.add(checkBox);
        panel.add(panelA);

        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(5, 0, 9999, 1);
        spinner = new JSpinner(spinnerNumberModel);
        spinner.setPreferredSize(new Dimension(50, 20));
        spinner.setVisible(false);

        JPanel panelB = new JPanel();
        panelB.setAlignmentX(LEFT_ALIGNMENT);
        panelB.add(spinner);
        panel.add(panelB);

        button = new JButton("OK");
        JPanel panelC = new JPanel();
        panelC.setAlignmentX(LEFT_ALIGNMENT);
        panelC.add(button);
        button.setVisible(false);
        panel.add(panelC);

        add(panel);
        setListeners();
    }

    private synchronized int getTimeValue() {
        return TIME_TO_SHOW;
    }

    private synchronized void updateAfterClick() {
        filteredThroughputUsing.clear();
        for (int i = 0; i < throughputUsing.getItemCount(); i++) {
            filteredThroughputUsing.add(throughputUsing.getDataItem(i));
        }
    }

    private void setListeners() {
        checkBox.addActionListener(e -> {
            if (checkBox.isSelected()) {
                spinner.setVisible(true);
                button.setVisible(true);
            } else {
                spinner.setVisible(false);
                button.setVisible(false);
            }
        });

        button.addActionListener(e -> SwingUtilities.invokeLater(() -> {
            TIME_TO_SHOW = (Integer) spinner.getValue();
            updateAfterClick();
        }));
    }

    public XYSeriesCollection getDataSet() {
        return dataSet;
    }

    public XYSeriesCollection setDataSet(ArrayList<Integer> throughput, ArrayList<BigDecimal> time) {
        dataSet = null;
        dataSet = new XYSeriesCollection();
        for (int i = 0; i < throughput.size(); i++) {
            throughputUsing.add(throughput.get(i), new Double(time.get(i).doubleValue()));
            filteredThroughputUsing.add(throughput.get(i), new Double(time.get(i).doubleValue()));
        }
        return dataSet;
    }

    public void updateChart(BigDecimal throughput, BigDecimal time) {
        throughputUsing.add(new Double(time.doubleValue()), new Double(throughput.doubleValue()));
        filteredThroughputUsing.add(new Double(time.doubleValue()), new Double(throughput.doubleValue()));
        if (getTimeValue() != Integer.MAX_VALUE) {
            for (int i = 0; i < filteredThroughputUsing.getItemCount(); i++) {
                if (filteredThroughputUsing.getMaxX()
                        - filteredThroughputUsing.getDataItem(i).getXValue() > getTimeValue())
                    filteredThroughputUsing.remove(i);
            }
        }
    }

    void clearDataSet() {
        throughputUsing.clear();
        filteredThroughputUsing.clear();
    }

    void saveAsImg() {
        HistogramSaver.save(lineChart, CHART_TITLE + ".png", 600, 300);
    }
}
