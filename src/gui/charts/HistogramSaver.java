package gui.charts;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

import java.io.File;
import java.io.IOException;

public class HistogramSaver {
    public static void save(JFreeChart chart, String name, int width, int height) {
        try {
            ChartUtilities.saveChartAsPNG(new File(name), chart, width, height);
        } catch (IOException e) {
            System.err.println("Nie udało się zapisać histogramu do pliku");
        }
    }
}
