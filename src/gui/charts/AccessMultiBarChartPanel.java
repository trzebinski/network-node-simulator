package gui.charts;

import core.Switch;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;

public class AccessMultiBarChartPanel extends JPanel {
    private static final String ACCEPT = "przyjęte";
    private static final String DENY = "odrzucone";
    private static final String PRIORITY_NUMBER = "Priorytet nr ";
    private DefaultCategoryDataset dataset;
    private JFreeChart barChart;
    private String chartTitle;

    public AccessMultiBarChartPanel(String chartTitle) {
        this.chartTitle = chartTitle;

        initializeDataSet();

        barChart = ChartFactory.createBarChart(
                chartTitle,
                "",
                "procent pakietów",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
        add(chartPanel);
    }

    private void initializeDataSet() {
        this.dataset = new DefaultCategoryDataset();

        for (int i = 0; i < Switch.PRIORITY_CLASS_COUNT; i++) {
            dataset.addValue(0L, ACCEPT, PRIORITY_NUMBER + (i + 1));
            dataset.addValue(0L, DENY, PRIORITY_NUMBER + (i + 1));
        }
    }

    public void updateStatisticsOfAccessControl(long[] accessAllowCount, long[] accessDenyCount) {
        for (int i = 0; i < Switch.PRIORITY_CLASS_COUNT; i++) {
            double all = accessAllowCount[i] + accessDenyCount[i];
            double allowPercent = accessAllowCount[i] / all * 100;
            double denyPercent = accessDenyCount[i] / all * 100;

            dataset.setValue(allowPercent, ACCEPT, PRIORITY_NUMBER + (i + 1));
            dataset.setValue(denyPercent, DENY, PRIORITY_NUMBER + (i + 1));
        }
    }

    public void saveAsImg() {
        HistogramSaver.save(barChart, chartTitle + ".png", 600, 300);
    }
}
