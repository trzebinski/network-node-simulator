package gui.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;

public class AccessBarChartPanel extends JPanel {
    private static final String COLUMN_ACCEPT = "Przyjęte";
    private static final String COLUMN_DENY = "Odrzucone";
    private static final String ROW_VALUE = "Wartość";
    private DefaultCategoryDataset dataSet;
    private JFreeChart barChart;
    private String chartTitle;

    public AccessBarChartPanel(String chartTitle) {
        this.chartTitle = chartTitle;

        initializeDataSet();

        barChart = ChartFactory.createBarChart(
                chartTitle,
                "",
                "ilość pakietów",
                dataSet,
                PlotOrientation.VERTICAL,
                false, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
        add(chartPanel);
    }

    private void initializeDataSet() {
        this.dataSet = new DefaultCategoryDataset();

        dataSet.addValue(0L, ROW_VALUE, COLUMN_ACCEPT);
        dataSet.addValue(0L, ROW_VALUE, COLUMN_DENY);
    }

    public void updateStatisticsOfAccessControl(long accessAllowCount, long accessDenyCount) {
        dataSet.setValue(accessAllowCount, ROW_VALUE, COLUMN_ACCEPT);
        dataSet.setValue(accessDenyCount, ROW_VALUE, COLUMN_DENY);
    }

    public void saveAsImg() {
        HistogramSaver.save(barChart, chartTitle + ".png", 600, 300);
    }
}
