package gui.charts;

import core.Main;
import core.Request;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.math.BigDecimal;
import java.util.ArrayList;

class WaitingTimeChartPanel extends JPanel {
    private static final String[] PRIORITY = {"1", "2", "3", "4", "5"};
    private static final String ROW_VALUE = "Wartość";
    private ArrayList<BigDecimal> sumOfWaitingTime;
    private ArrayList<Integer> sumOfRequests;
    private DefaultCategoryDataset dataSet;
    private JFreeChart barChart;
    private String chartTitle;

    WaitingTimeChartPanel(String title) {
        this.chartTitle = title;

        initializeDataSet();

        barChart = ChartFactory.createBarChart(
                chartTitle,
                "priorytety pakietów",
                "czas oczekiwania",
                dataSet,
                PlotOrientation.VERTICAL,
                false, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 300));
        add(chartPanel);
        saveChartToFile();
    }

    private void createGraph() {
        for (Request request : Main.simulation.getSwitch().getExecutedRequest()) {
            int priorityIndex = request.getPriority() - 1;
            sumOfWaitingTime.set(priorityIndex, sumOfWaitingTime.get(priorityIndex).add(BigDecimal.valueOf(
                    request.getWaitingTime())));
            sumOfRequests.set(priorityIndex, sumOfRequests.get(priorityIndex) + 1);
        }

        ArrayList<BigDecimal> avgWaitingTime = new ArrayList<>();

        for (int i = 0; i < PRIORITY.length; i++) {
            if (sumOfRequests.get(i) > 0) {
                avgWaitingTime.add(sumOfWaitingTime.get(i).divide(BigDecimal.valueOf(
                        sumOfRequests.get(i)), BigDecimal.ROUND_HALF_UP));
                dataSet.addValue(avgWaitingTime.get(i), ROW_VALUE, PRIORITY[i]);
            }
        }
    }

    private void initializeDataSet() {
        this.dataSet = new DefaultCategoryDataset();

        this.sumOfWaitingTime = new ArrayList<>();
        this.sumOfRequests = new ArrayList<>();

        for (String s : PRIORITY) {
            dataSet.addValue(0L, ROW_VALUE, s);
            sumOfWaitingTime.add(BigDecimal.valueOf(0L));
            sumOfRequests.add(0);
        }

        createGraph();
    }

    private void saveChartToFile() {
        HistogramSaver.save(barChart, chartTitle + ".png", 600, 300);
    }
}
