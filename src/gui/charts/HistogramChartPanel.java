package gui.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SeriesRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

import javax.swing.*;
import java.awt.*;

class HistogramChartPanel extends JPanel {
    private ChartPanel chartPanel;
    private Color greenColor = new Color(0, 153, 0);
    private String chartTitle;
    private JFreeChart mainChart;
    private HistogramDataset histogramDataSet;

    HistogramChartPanel(String chartTitle, double[] dataSet, double[] allowDataSet) {
        this.chartTitle = chartTitle;

        initializeDataSet(dataSet, allowDataSet);
        initializeMainChart();
        setupChartWith2DataSets();
        saveChartToFile();

        chartPanel = new ChartPanel(mainChart);
        add(chartPanel);
    }

    private void initializeMainChart() {
        String plotTitle = this.chartTitle;
        String xaxis = "czas w sekundach";
        String yaxis = "ilość zapytań";

        PlotOrientation plotOrientation = PlotOrientation.VERTICAL;
        mainChart = ChartFactory.createHistogram(plotTitle, xaxis, yaxis, histogramDataSet, plotOrientation,
                true, true, false);
    }

    private void initializeDataSet(double[] dataSet, double[] allowDataSet) {
        int numberOfBins = (int) dataSet[dataSet.length - 1] * 25;

        if (numberOfBins < 1)
            numberOfBins = 1;

        histogramDataSet = new HistogramDataset();
        histogramDataSet.setType(HistogramType.FREQUENCY);

        if (dataSet.length > 0)
            histogramDataSet.addSeries("wszystkie pakiety", dataSet, numberOfBins);

        if (allowDataSet != null && allowDataSet.length > 0)
            histogramDataSet.addSeries("zaakceptowane pakiety", allowDataSet, numberOfBins);
    }

    private void saveChartToFile() {
        int saveImgWidth = 900;
        int saveImgHeight = 500;
        HistogramSaver.save(mainChart, chartTitle + ".png", saveImgWidth, saveImgHeight);
    }

    private void setupChartWith2DataSets() {
        XYPlot plot = (XYPlot) mainChart.getPlot();
        plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
        plot.setDomainPannable(true);
        plot.setRangePannable(true);

        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(1, greenColor);
        renderer.setDrawBarOutline(false);
        renderer.setBarPainter(new StandardXYBarPainter());
        renderer.setShadowVisible(false);
    }

    void setSizeOfChartPanel(int width, int height) {
        chartPanel.setSize(width, height);
        chartPanel.setPreferredSize(new Dimension(width, height));
    }

    void setDisplayLegend() {
        mainChart.getLegend().visible = false;
    }
}
