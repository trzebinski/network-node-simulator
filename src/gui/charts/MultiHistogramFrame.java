package gui.charts;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class MultiHistogramFrame extends JFrame {
    public MultiHistogramFrame(List<double[]> listOfListAllRequests, List<double[]> listOfListAllowRequests) {
        super("Histogramy ruchu w zależności od priorytetu");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(800, 800);
        setLocation(150, 150);
        JPanel mainPanel = getMainJPanel(listOfListAllRequests, listOfListAllowRequests);

        JScrollPane scrollPane = new JScrollPane(mainPanel);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        setVisible(true);
    }

    private JPanel getMainJPanel(List<double[]> listOfListAllRequests, List<double[]> listOfListAllowRequests) {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(5, 1));

        HistogramChartPanel[] histogramChartPanel = new HistogramChartPanel[5];
        for (int i = 0; i < histogramChartPanel.length; i++) {
            if (listOfListAllRequests.get(i).length < 1)
                continue;

            HistogramChartPanel actualChartPanel = new HistogramChartPanel("Histogram czasu pojawiania się " +
                    "pakietów dla priorytetu nr " + (i + 1), listOfListAllRequests.get(i),
                    listOfListAllowRequests.get(i));
            actualChartPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            histogramChartPanel[i] = actualChartPanel;
            int HEIGHT_OF_ONE_CHART = 250;
            actualChartPanel.setSizeOfChartPanel(500, HEIGHT_OF_ONE_CHART);
            actualChartPanel.setDisplayLegend();
            mainPanel.add(actualChartPanel);
        }
        return mainPanel;
    }
}
