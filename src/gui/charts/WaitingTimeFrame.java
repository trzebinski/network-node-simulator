package gui.charts;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class WaitingTimeFrame extends JFrame {
    public WaitingTimeFrame() {
        super("Średni czas oczekiwania na wykonanie");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel waitingTimeChartPanel = new WaitingTimeChartPanel("Średni czas oczekiwania na wykonanie");
        waitingTimeChartPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(waitingTimeChartPanel);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
