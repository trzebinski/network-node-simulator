package gui.charts;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class HistogramFrame extends JFrame {
    public HistogramFrame(double[] dataset, double[] datasetAllow) {
        super("Histogram czasu pojawienia się pakietów");

        HistogramChartPanel histogramChartPanel = new HistogramChartPanel("Histogram czasu pojawiania się " +
                "pakietów", dataset, datasetAllow);
        histogramChartPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(histogramChartPanel);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
