package gui.charts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

public class AccessLineChartPanel extends JPanel {
    private static final String ACCEPT = "przyjęte";
    private static final String DENY = "odrzucone";
    private JFreeChart lineChart;
    private XYSeries dataSetAllow;
    private XYSeries dataSetDeny;
    private XYSeriesCollection dataSetSeriesAllow;
    private XYSeriesCollection dataSetSeriesDeny;
    private boolean isPercent = true;
    private String chartTitle;

    public AccessLineChartPanel(String chartTitle) {
        this.chartTitle = chartTitle;

        initializeDataSet();

        lineChart = ChartFactory.createXYLineChart(
                chartTitle,
                "czas",
                (isPercent ? "procent" : "liczba") + " przyjętych",
                dataSetSeriesAllow,
                PlotOrientation.VERTICAL,
                true, true, false);

        XYPlot xyPlot = lineChart.getXYPlot();
        xyPlot.mapDatasetToRangeAxis(1, 1);

        final NumberAxis numberAxis = new NumberAxis((isPercent ? "procent" : "liczba") + " odrzuconych");
        xyPlot.setRangeAxis(1, numberAxis);
        xyPlot.setDataset(1, dataSetSeriesDeny);
        xyPlot.mapDatasetToRangeAxis(1, 1);

        final StandardXYItemRenderer rendererRed = new StandardXYItemRenderer();
        rendererRed.setSeriesPaint(0, Color.RED);

        final StandardXYItemRenderer rendererGreen = new StandardXYItemRenderer();
        Color greenColor = new Color(0, 153, 0);
        rendererGreen.setSeriesPaint(0, greenColor);
        xyPlot.setRenderer(0, rendererGreen);
        xyPlot.setRenderer(1, rendererRed);

        ChartPanel chart = new ChartPanel(lineChart);
        chart.setPreferredSize(new Dimension(600, 300));
        add(chart);
    }

    private void initializeDataSet() {
        dataSetSeriesAllow = new XYSeriesCollection();
        dataSetSeriesDeny = new XYSeriesCollection();

        dataSetAllow = new XYSeries(ACCEPT);
        dataSetDeny = new XYSeries(DENY);

        dataSetSeriesDeny.addSeries(dataSetDeny);
        dataSetSeriesAllow.addSeries(dataSetAllow);
    }

    public void addStatisticsOfAccessControl(int accessAllowCount, int accessDenyCount, BigDecimal time) {
        double doubleValue = (time.doubleValue());

        if (isPercent) {
            int allCount = accessAllowCount + accessDenyCount;
            double percentAllow = accessAllowCount / (double) allCount * 100;

            dataSetAllow.add(doubleValue, percentAllow);
            dataSetDeny.add(doubleValue, 100.0 - percentAllow);
        } else {
            dataSetAllow.add(doubleValue, accessAllowCount);
            dataSetDeny.add(doubleValue, accessDenyCount);
        }
    }

    public void clearDataSet() {
        dataSetAllow.clear();
        dataSetDeny.clear();
    }

    public void saveAsImg() {
        HistogramSaver.save(lineChart, chartTitle + ".png", 600, 300);
    }
}
