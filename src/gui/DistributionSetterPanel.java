package gui;

import jdistlib.Exponential;
import jdistlib.LogNormal;
import jdistlib.Uniform;
import jdistlib.evd.GeneralizedPareto;
import jdistlib.generic.GenericDistribution;
import statistics.InputData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Objects;

class DistributionSetterPanel extends JPanel {
    private static String[] DISTRIBUTION_MENU = {"Eksponencjalny", "Log-Normalny", "Jednostajny", "UogólnionyPareto"};
    private JComboBox<String> comboBox;
    private JTextField parameter1TextField;
    private JTextField parameter2TextField;
    private JTextField parameter3TextField;

    DistributionSetterPanel() {
        initialize();
    }

    private NumberFormat getNumberFormat() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setGroupingUsed(false);
        numberFormat.setMinimumFractionDigits(1);
        numberFormat.setMinimumIntegerDigits(1);
        return numberFormat;
    }

    private double getNumberParam(JTextField textField) {
        if (textField.getText() == null || textField.getText().equals("")) {
            return 0.0;
        } else {
            return Double.parseDouble(textField.getText().replace(',', '.'));
        }
    }

    private void initialize() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);

        comboBox = new JComboBox<>(DISTRIBUTION_MENU);
        setComboboxListener(comboBox);
        GridBagConstraints comboBox = new GridBagConstraints();
        comboBox.anchor = GridBagConstraints.EAST;
        comboBox.gridx = 0;
        comboBox.gridy = 0;
        this.add(this.comboBox, comboBox);
        InputData.COMBO_BOX_DISTRIBUTION_VALUES.add(this.comboBox);

        parameter1TextField = new JFormattedTextField(getNumberFormat());
        GridBagConstraints parameter1 = new GridBagConstraints();
        parameter1.fill = GridBagConstraints.HORIZONTAL;
        parameter1.gridx = 1;
        parameter1.gridy = 0;
        this.add(parameter1TextField, parameter1);
        int columnOfTextField = 3;
        parameter1TextField.setColumns(columnOfTextField);
        parameter1TextField.setText("1,0");
        InputData.TEXT_FIELD_DISTRIBUTION_VALUES.add(parameter1TextField);

        parameter2TextField = new JFormattedTextField(getNumberFormat());
        GridBagConstraints parameter2 = new GridBagConstraints();
        parameter2.fill = GridBagConstraints.HORIZONTAL;
        parameter2.gridx = 2;
        parameter2.gridy = 0;
        this.add(parameter2TextField, parameter2);
        parameter2TextField.setColumns(columnOfTextField);
        parameter2TextField.setText("1,0");
        InputData.TEXT_FIELD_DISTRIBUTION_VALUES.add(parameter2TextField);

        parameter3TextField = new JFormattedTextField(getNumberFormat());
        GridBagConstraints parameter3 = new GridBagConstraints();
        parameter3.fill = GridBagConstraints.HORIZONTAL;
        parameter3.gridx = 3;
        parameter3.gridy = 0;
        this.add(parameter3TextField, parameter3);
        parameter3TextField.setColumns(columnOfTextField);
        parameter3TextField.setText("1,0");
        InputData.TEXT_FIELD_DISTRIBUTION_VALUES.add(parameter3TextField);

        this.comboBox.setSelectedIndex(0);
    }

    private void setComboboxListener(JComboBox<String> combobox) {
        ActionListener itemListener = itemEvent -> {
            switch ((String) Objects.requireNonNull(comboBox.getSelectedItem())) {
                case "Eksponencjalny":
                    parameter2TextField.setEnabled(false);
                    parameter3TextField.setEnabled(false);
                    break;
                case "Log-Normalny":
                    parameter2TextField.setEnabled(true);
                    parameter3TextField.setEnabled(false);
                    break;
                case "Jednostajny":
                    parameter2TextField.setEnabled(true);
                    parameter3TextField.setEnabled(false);
                    break;
                case "UogólnionyPareto":
                    parameter2TextField.setEnabled(true);
                    parameter3TextField.setEnabled(true);
                    break;
            }
        };
        combobox.addActionListener(itemListener);
    }

    GenericDistribution getDistributionGenerator() {
        switch ((String) Objects.requireNonNull(comboBox.getSelectedItem())) {
            case "Eksponencjalny":
                return new Exponential(getNumberParam(parameter1TextField));
            case "Log-Normalny":
                return new LogNormal(getNumberParam(parameter1TextField), getNumberParam(parameter2TextField));
            case "Jednostajny":
                return new Uniform(getNumberParam(parameter1TextField), getNumberParam(parameter2TextField));
            case "UogólnionyPareto":
                return new GeneralizedPareto(getNumberParam(parameter1TextField), getNumberParam(parameter2TextField),
                        getNumberParam(parameter3TextField));
            default:
                return null;
        }
    }
}
