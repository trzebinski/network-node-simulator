package gui;

import algorithms.AccessControlAlgorithm;
import statistics.InputData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Objects;

class AlgorithmPanel extends JPanel {
    private static String[] ACCESS_CONTROL_MENU = {"Wybierz", "RED", "RIO", "WRED"};
    private GUI gui;
    private JComboBox chosenAccessControlAlgorithm;
    private JComboBox chosenLoadParameters;
    private JComboBox chosenQueuingAlgorithm;
    private JTextField maxth1TextField;
    private JTextField maxth2TextField;
    private JTextField maxth3TextField;
    private JTextField maxth4TextField;
    private JTextField maxth5TextField;
    private JTextField minth1TextField;
    private JTextField minth2TextField;
    private JTextField minth3TextField;
    private JTextField minth4TextField;
    private JTextField minth5TextField;

    AlgorithmPanel(GUI gui) {
        this.gui = gui;

        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);

        JLabel queueLabel = new JLabel("Algorytm kolejkowania:");
        GridBagConstraints queue = new GridBagConstraints();
        queue.insets = new Insets(0, 0, 5, 5);
        queue.gridx = 0;
        queue.gridy = 0;
        this.add(queueLabel, queue);
        chosenQueuingAlgorithm = new JComboBox();
        GridBagConstraints queueComboBox = new GridBagConstraints();
        queueComboBox.insets = new Insets(0, 0, 5, 5);
        queueComboBox.gridx = 1;
        queueComboBox.gridy = 0;
        chosenQueuingAlgorithm.addItem("FIFO");
        chosenQueuingAlgorithm.addItem("HTB");
        chosenQueuingAlgorithm.addItem("PQ");
        chosenQueuingAlgorithm.addItem("SJF");
        chosenQueuingAlgorithm.addItem("WFQ");
        this.add(chosenQueuingAlgorithm, queueComboBox);

        JLabel accessAlgorithmLabel = new JLabel("Algorytm kontroli dostępu:");
        GridBagConstraints accessAlgorithm = new GridBagConstraints();
        accessAlgorithm.anchor = GridBagConstraints.WEST;
        accessAlgorithm.insets = new Insets(0, 15, 5, 5);
        accessAlgorithm.gridx = 0;
        accessAlgorithm.gridy = 1;
        this.add(accessAlgorithmLabel, accessAlgorithm);
        chosenAccessControlAlgorithm = new JComboBox<>(ACCESS_CONTROL_MENU);
        GridBagConstraints accessAlgorithmComboBox = new GridBagConstraints();
        accessAlgorithmComboBox.insets = new Insets(0, 0, 5, 5);
        accessAlgorithmComboBox.gridx = 1;
        accessAlgorithmComboBox.gridy = 1;
        this.add(chosenAccessControlAlgorithm, accessAlgorithmComboBox);
        setComboboxListener(chosenAccessControlAlgorithm);

        JLabel loadParametersLabel = new JLabel("Wczytaj zestaw parametrów:");
        GridBagConstraints loadParameters = new GridBagConstraints();
        loadParameters.anchor = GridBagConstraints.WEST;
        loadParameters.insets = new Insets(0, 15, 5, 5);
        loadParameters.gridx = 0;
        loadParameters.gridy = 2;
        this.add(loadParametersLabel, loadParameters);
        chosenLoadParameters = new JComboBox();
        GridBagConstraints loadParametersComboBox = new GridBagConstraints();
        loadParametersComboBox.insets = new Insets(0, 0, 5, 5);
        loadParametersComboBox.gridx = 1;
        loadParametersComboBox.gridy = 2;
        chosenLoadParameters.addItem("Wybierz");

        JLabel maxth1Label = new JLabel("maxth1:");
        GridBagConstraints maxth1 = new GridBagConstraints();
        maxth1.anchor = GridBagConstraints.WEST;
        maxth1.insets = new Insets(0, 15, 5, 5);
        maxth1.gridx = 0;
        maxth1.gridy = 4;
        this.add(maxth1Label, maxth1);
        maxth1TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMaxth1 = new GridBagConstraints();
        gridBagConstraintsMaxth1.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMaxth1.gridx = 1;
        gridBagConstraintsMaxth1.gridy = 4;
        this.add(maxth1TextField, gridBagConstraintsMaxth1);
        int columnOfTextField = 3;
        maxth1TextField.setColumns(columnOfTextField);
        maxth1TextField.setText("50");
        maxth1TextField.setEnabled(false);
        AccessControlAlgorithm.MAXTH_VALUES.add(maxth1TextField);

        JLabel minth1Label = new JLabel("minth1:");
        GridBagConstraints minth1 = new GridBagConstraints();
        minth1.anchor = GridBagConstraints.WEST;
        minth1.insets = new Insets(0, 15, 5, 5);
        minth1.gridx = 0;
        minth1.gridy = 5;
        this.add(minth1Label, minth1);
        minth1TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMinth1 = new GridBagConstraints();
        gridBagConstraintsMinth1.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMinth1.gridx = 1;
        gridBagConstraintsMinth1.gridy = 5;
        this.add(minth1TextField, gridBagConstraintsMinth1);
        minth1TextField.setColumns(columnOfTextField);
        minth1TextField.setText("10");
        minth1TextField.setEnabled(false);
        AccessControlAlgorithm.MINTH_VALUES.add(minth1TextField);

        JLabel maxth2Label = new JLabel("maxth2:");
        GridBagConstraints maxth2 = new GridBagConstraints();
        maxth2.anchor = GridBagConstraints.WEST;
        maxth2.insets = new Insets(0, 15, 5, 5);
        maxth2.gridx = 0;
        maxth2.gridy = 6;
        this.add(maxth2Label, maxth2);
        maxth2TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMaxth2 = new GridBagConstraints();
        gridBagConstraintsMaxth2.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMaxth2.gridx = 1;
        gridBagConstraintsMaxth2.gridy = 6;
        this.add(maxth2TextField, gridBagConstraintsMaxth2);
        maxth2TextField.setColumns(columnOfTextField);
        maxth2TextField.setText("70");
        maxth2TextField.setEnabled(false);
        AccessControlAlgorithm.MAXTH_VALUES.add(maxth2TextField);

        JLabel minth2Label = new JLabel("minth2:");
        GridBagConstraints minth2 = new GridBagConstraints();
        minth2.anchor = GridBagConstraints.WEST;
        minth2.insets = new Insets(0, 15, 5, 5);
        minth2.gridx = 0;
        minth2.gridy = 7;
        this.add(minth2Label, minth2);
        minth2TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMinth2 = new GridBagConstraints();
        gridBagConstraintsMinth2.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMinth2.gridx = 1;
        gridBagConstraintsMinth2.gridy = 7;
        this.add(minth2TextField, gridBagConstraintsMinth2);
        minth2TextField.setColumns(columnOfTextField);
        minth2TextField.setText("30");
        minth2TextField.setEnabled(false);
        AccessControlAlgorithm.MINTH_VALUES.add(minth2TextField);

        JLabel maxth3Label = new JLabel("maxth3:");
        GridBagConstraints maxth3 = new GridBagConstraints();
        maxth3.anchor = GridBagConstraints.WEST;
        maxth3.insets = new Insets(0, 15, 5, 5);
        maxth3.gridx = 0;
        maxth3.gridy = 8;
        this.add(maxth3Label, maxth3);
        maxth3TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMaxth3 = new GridBagConstraints();
        gridBagConstraintsMaxth3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMaxth3.gridx = 1;
        gridBagConstraintsMaxth3.gridy = 8;
        this.add(maxth3TextField, gridBagConstraintsMaxth3);
        maxth3TextField.setColumns(columnOfTextField);
        maxth3TextField.setText("75");
        maxth3TextField.setEnabled(false);
        AccessControlAlgorithm.MAXTH_VALUES.add(maxth3TextField);

        JLabel minth3Label = new JLabel("minth3:");
        GridBagConstraints minth3 = new GridBagConstraints();
        minth3.anchor = GridBagConstraints.WEST;
        minth3.insets = new Insets(0, 15, 5, 5);
        minth3.gridx = 0;
        minth3.gridy = 9;
        this.add(minth3Label, minth3);
        minth3TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMinth3 = new GridBagConstraints();
        gridBagConstraintsMinth3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMinth3.gridx = 1;
        gridBagConstraintsMinth3.gridy = 9;
        this.add(minth3TextField, gridBagConstraintsMinth3);
        minth3TextField.setColumns(columnOfTextField);
        minth3TextField.setText("50");
        minth3TextField.setEnabled(false);
        AccessControlAlgorithm.MINTH_VALUES.add(minth3TextField);

        JLabel maxth4Label = new JLabel("maxth4:");
        GridBagConstraints maxth4 = new GridBagConstraints();
        maxth4.anchor = GridBagConstraints.WEST;
        maxth4.insets = new Insets(0, 15, 5, 5);
        maxth4.gridx = 0;
        maxth4.gridy = 10;
        this.add(maxth4Label, maxth4);
        maxth4TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMaxth4 = new GridBagConstraints();
        gridBagConstraintsMaxth4.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMaxth4.gridx = 1;
        gridBagConstraintsMaxth4.gridy = 10;
        this.add(maxth4TextField, gridBagConstraintsMaxth4);
        maxth4TextField.setColumns(columnOfTextField);
        maxth4TextField.setText("80");
        maxth4TextField.setEnabled(false);
        AccessControlAlgorithm.MAXTH_VALUES.add(maxth4TextField);

        JLabel minth4Label = new JLabel("minth4:");
        GridBagConstraints minth4 = new GridBagConstraints();
        minth4.anchor = GridBagConstraints.WEST;
        minth4.insets = new Insets(0, 15, 5, 5);
        minth4.gridx = 0;
        minth4.gridy = 11;
        this.add(minth4Label, minth4);
        minth4TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMinth4 = new GridBagConstraints();
        gridBagConstraintsMinth4.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMinth4.gridx = 1;
        gridBagConstraintsMinth4.gridy = 11;
        this.add(minth4TextField, gridBagConstraintsMinth4);
        minth4TextField.setColumns(columnOfTextField);
        minth4TextField.setText("60");
        minth4TextField.setEnabled(false);
        AccessControlAlgorithm.MINTH_VALUES.add(minth4TextField);

        JLabel maxth5Label = new JLabel("maxth5:");
        GridBagConstraints maxth5 = new GridBagConstraints();
        maxth5.anchor = GridBagConstraints.WEST;
        maxth5.insets = new Insets(0, 15, 5, 5);
        maxth5.gridx = 0;
        maxth5.gridy = 12;
        this.add(maxth5Label, maxth5);
        maxth5TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMaxth5 = new GridBagConstraints();
        gridBagConstraintsMaxth5.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMaxth5.gridx = 1;
        gridBagConstraintsMaxth5.gridy = 12;
        this.add(maxth5TextField, gridBagConstraintsMaxth5);
        maxth5TextField.setColumns(columnOfTextField);
        maxth5TextField.setText("100");
        maxth5TextField.setEnabled(false);
        AccessControlAlgorithm.MAXTH_VALUES.add(maxth5TextField);

        JLabel minth5Label = new JLabel("minth5:");
        GridBagConstraints minth5 = new GridBagConstraints();
        minth5.anchor = GridBagConstraints.WEST;
        minth5.insets = new Insets(0, 15, 5, 5);
        minth5.gridx = 0;
        minth5.gridy = 13;
        this.add(minth5Label, minth5);
        minth5TextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsMinth5 = new GridBagConstraints();
        gridBagConstraintsMinth5.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsMinth5.gridx = 1;
        gridBagConstraintsMinth5.gridy = 13;
        this.add(minth5TextField, gridBagConstraintsMinth5);
        minth5TextField.setColumns(columnOfTextField);
        minth5TextField.setText("80");
        minth5TextField.setEnabled(false);
        AccessControlAlgorithm.MINTH_VALUES.add(minth5TextField);

        JLabel probabilityLabel = new JLabel("prawd. odrzuceń:");
        GridBagConstraints probability = new GridBagConstraints();
        probability.anchor = GridBagConstraints.WEST;
        probability.insets = new Insets(0, 15, 5, 5);
        probability.gridx = 0;
        probability.gridy = 14;
        this.add(probabilityLabel, probability);
        JTextField probabilityTextField = new JFormattedTextField();
        GridBagConstraints gridBagConstraintsProbability = new GridBagConstraints();
        gridBagConstraintsProbability.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsProbability.gridx = 1;
        gridBagConstraintsProbability.gridy = 14;
        this.add(probabilityTextField, gridBagConstraintsProbability);
        probabilityTextField.setColumns(columnOfTextField);
        probabilityTextField.setText("0.4");
        AccessControlAlgorithm.PROBABILITY_VALUES.add(probabilityTextField);

        InputData inputData = new InputData(gui);
        inputData.readData();
        int parametersQuantity = inputData.distributionParameters.size() / 5;
        for (int i = 1; i <= parametersQuantity; i++) {
            chosenLoadParameters.addItem(i);
        }
        this.add(chosenLoadParameters, loadParametersComboBox);
        chosenLoadParameters.addActionListener(e -> inputData.importData(Objects.requireNonNull(
                chosenLoadParameters.getSelectedItem()).toString()));
    }

    private void setComboboxListener(JComboBox<String> comboBox) {
        ActionListener actionListener = itemEvent -> {
            switch ((String) Objects.requireNonNull(comboBox.getSelectedItem())) {
                case "RED":
                    maxth1TextField.setEnabled(true);
                    minth1TextField.setEnabled(true);
                    maxth2TextField.setEnabled(false);
                    minth2TextField.setEnabled(false);
                    maxth3TextField.setEnabled(false);
                    minth3TextField.setEnabled(false);
                    maxth4TextField.setEnabled(false);
                    minth4TextField.setEnabled(false);
                    maxth5TextField.setEnabled(false);
                    minth5TextField.setEnabled(false);
                    break;
                case "RIO":
                    maxth1TextField.setEnabled(true);
                    minth1TextField.setEnabled(true);
                    maxth2TextField.setEnabled(true);
                    minth2TextField.setEnabled(true);
                    maxth3TextField.setEnabled(false);
                    minth3TextField.setEnabled(false);
                    maxth4TextField.setEnabled(false);
                    minth4TextField.setEnabled(false);
                    maxth5TextField.setEnabled(false);
                    minth5TextField.setEnabled(false);
                    break;
                case "WRED":
                    maxth1TextField.setEnabled(true);
                    minth1TextField.setEnabled(true);
                    maxth2TextField.setEnabled(true);
                    minth2TextField.setEnabled(true);
                    maxth3TextField.setEnabled(true);
                    minth3TextField.setEnabled(true);
                    maxth4TextField.setEnabled(true);
                    minth4TextField.setEnabled(true);
                    maxth5TextField.setEnabled(true);
                    minth5TextField.setEnabled(true);
                    break;
                case "Wybierz":
                    maxth1TextField.setEnabled(false);
                    minth1TextField.setEnabled(false);
                    maxth2TextField.setEnabled(false);
                    minth2TextField.setEnabled(false);
                    maxth3TextField.setEnabled(false);
                    minth3TextField.setEnabled(false);
                    maxth4TextField.setEnabled(false);
                    minth4TextField.setEnabled(false);
                    maxth5TextField.setEnabled(false);
                    minth5TextField.setEnabled(false);
                    break;
            }
        };
        comboBox.addActionListener(actionListener);
    }

    public GUI getGui() {
        return gui;
    }

    String getChosenAccessControlAlgorithm() {
        return (String) chosenAccessControlAlgorithm.getSelectedItem();
    }

    String getChosenQueuingAlgorithm() {
        return (String) chosenQueuingAlgorithm.getSelectedItem();
    }
}
