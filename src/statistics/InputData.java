package statistics;

import gui.GUI;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.swing.*;
import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class InputData {
    private ArrayList<Coefficients> coefficients = new ArrayList<>();
    private GUI gui;
    private int[] classes = {0, 0, 0, 0, 0};
    private String path = Paths.get("results\\inputData.txt").toString();
    public static ArrayList<JComboBox<String>> COMBO_BOX_DISTRIBUTION_VALUES = new ArrayList<>();
    public static ArrayList<JTextField> TEXT_FIELD_DISTRIBUTION_VALUES = new ArrayList<>();
    public static ArrayList<JSpinner> SPINNER_LISTS = new ArrayList<>();
    public static boolean WHETHER_PARAMETERS_LOADED = false;
    public ArrayList<String> distributionParameters = new ArrayList<>();

    class Coefficients {
        String valueCB;
        String value1;
        String value2;
        String value3;

        Coefficients(String valueCB, String value1, String value2, String value3) {
            this.valueCB = valueCB;
            this.value1 = value1;
            this.value2 = value2;
            this.value3 = value3;
        }
    }

    public InputData(GUI gui) {
        this.gui = gui;
    }

    public void allData() {
        if (!WHETHER_PARAMETERS_LOADED) {
            classes[0] = gui.getCountOfEachTrafficType().get(0);
            classes[1] = gui.getCountOfEachTrafficType().get(1);
            classes[2] = gui.getCountOfEachTrafficType().get(2);
            classes[3] = gui.getCountOfEachTrafficType().get(3);
            classes[4] = gui.getCountOfEachTrafficType().get(4);

            int j = 0;
            for (int i = 0; i < 15; i++) {
                String value1 = Objects.requireNonNull(COMBO_BOX_DISTRIBUTION_VALUES.get(i).getSelectedItem())
                        .toString();
                String value2 = TEXT_FIELD_DISTRIBUTION_VALUES.get(j).getText();
                j++;

                String value3 = TEXT_FIELD_DISTRIBUTION_VALUES.get(j).getText();
                j++;

                String value4 = TEXT_FIELD_DISTRIBUTION_VALUES.get(j).getText();
                j++;

                coefficients.add(new Coefficients(value1, value2, value3, value4));
            }
            writeAllData();
        }
    }

    public void importData(String id) {
        if (!id.equals("Wybierz")) {
            int params = Integer.parseInt(id);
            params--;
            WHETHER_PARAMETERS_LOADED = true;

            JSONParser parser = new JSONParser();
            int quantityClasses;

            int z = 0;
            int j = 0;
            int y = 0;
            for (int i = params * 5; i < params * 5 + 5; i++) {
                try {
                    Object obj = parser.parse(distributionParameters.get(i));
                    JSONObject jsonObject = (JSONObject) obj;

                    quantityClasses = Integer.parseInt(jsonObject.get("quantity").toString());
                    SPINNER_LISTS.get(z).setValue(quantityClasses);

                    COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(comboBoxPosition(jsonObject.get("alfa-cb")
                            .toString()));
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("alfa-v1").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("alfa-v2").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("alfa-v3").toString());
                    y++;
                    j++;

                    COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(comboBoxPosition(jsonObject.get("beta-cb")
                            .toString()));
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("beta-v1").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("beta-v2").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("beta-v3").toString());
                    y++;
                    j++;

                    COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(comboBoxPosition(jsonObject.get("gamma-cb")
                            .toString()));
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("gamma-v1").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("gamma-v2").toString());
                    y++;
                    TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText(jsonObject.get("gamma-v3").toString());
                    y++;
                    j++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                z++;
            }
        } else {
            WHETHER_PARAMETERS_LOADED = false;
            int j = 0;
            int y = 0;
            for (int i = 0; i < 5; i++) {
                SPINNER_LISTS.get(i).setValue(0);

                COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(0);
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                j++;

                COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(0);
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                j++;

                COMBO_BOX_DISTRIBUTION_VALUES.get(j).setSelectedIndex(0);
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                TEXT_FIELD_DISTRIBUTION_VALUES.get(y).setText("1.0");
                y++;
                j++;
            }
        }
    }

    public void readData() {
        distributionParameters.clear();

        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

            String getLine = bufferedReader.readLine();

            while (getLine != null) {
                distributionParameters.add(getLine);
                getLine = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private void writeAllData() {
        ArrayList<JSONObject> listObjects = new ArrayList<>();

        int j = 0;
        for (int i = 0; i < classes.length; i++) {
            JSONObject object = new JSONObject();

            object.put("class-id", i + 1);
            object.put("quantity", classes[i]);

            object.put("alfa-cb", coefficients.get(j).valueCB);
            object.put("alfa-v1", coefficients.get(j).value1);
            object.put("alfa-v2", coefficients.get(j).value2);
            object.put("alfa-v3", coefficients.get(j).value3);
            j++;

            object.put("beta-cb", coefficients.get(j).valueCB);
            object.put("beta-v1", coefficients.get(j).value1);
            object.put("beta-v2", coefficients.get(j).value2);
            object.put("beta-v3", coefficients.get(j).value3);
            j++;

            object.put("gamma-cb", coefficients.get(j).valueCB);
            object.put("gamma-v1", coefficients.get(j).value1);
            object.put("gamma-v2", coefficients.get(j).value2);
            object.put("gamma-v3", coefficients.get(j).value3);
            j++;

            listObjects.add(object);
        }

        try (FileWriter fileWriter = new FileWriter(path, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             PrintWriter printWriter = new PrintWriter(bufferedWriter)) {

            for (JSONObject listObject : listObjects) {
                StringWriter stringWriter = new StringWriter();
                listObject.writeJSONString(stringWriter);

                String jsonText = stringWriter.toString();
                printWriter.println(jsonText);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int comboBoxPosition(String string) {
        int i;

        switch (string) {
            case "Eksponencjalny":
                i = 0;
                break;
            case "Log-Normalny":
                i = 1;
                break;
            case "Jednostajny":
                i = 2;
                break;
            case "UogólnionyPareto":
                i = 3;
                break;
            default:
                i = 0;
                break;
        }
        return i;
    }
}
