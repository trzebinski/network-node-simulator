package algorithms;

import javax.swing.*;
import java.util.ArrayList;

public class AccessControlAlgorithm {
    private RedAlgorithm redAlgorithm;
    private RioAlgorithm rioAlgorithm;
    private WRedAlgorithm wRedAlgorithm;
    private String algorithm;
    public static ArrayList<JTextField> MAXTH_VALUES = new ArrayList<>();
    public static ArrayList<JTextField> MINTH_VALUES = new ArrayList<>();
    public static ArrayList<JTextField> PROBABILITY_VALUES = new ArrayList<>();

    public AccessControlAlgorithm(String algorithm) {
        this.redAlgorithm = new RedAlgorithm();
        this.rioAlgorithm = new RioAlgorithm();
        this.wRedAlgorithm = new WRedAlgorithm();
        this.algorithm = algorithm;
    }

    public boolean whetherReject(int queueSize, int priority) {
        boolean reject = true;

        switch (algorithm) {
            case "RED":
                reject = redAlgorithm.whetherReject(queueSize);
                break;
            case "RIO":
                reject = rioAlgorithm.whetherReject(queueSize, priority);
                break;
            case "WRED":
                reject = wRedAlgorithm.whetherReject(queueSize, priority);
                break;
        }
        return reject;
    }
}
