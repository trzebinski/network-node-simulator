package algorithms;

import core.Request;
import core.Simulation;

class WRedAlgorithm extends Auxiliary {
    private int count = -1;
    private double avg;
    private double m;
    private long maxth1;
    private long minth1;
    private long maxth2;
    private long minth2;
    private long maxth3;
    private long minth3;
    private long maxth4;
    private long minth4;
    private long maxth5;
    private long minth5;

    WRedAlgorithm() {
        this.avg = 0;
        this.m = 0;
        this.maxth1 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth1 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.maxth2 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(1).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth2 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(1).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.maxth3 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(2).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth3 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(2).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.maxth4 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(3).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth4 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(3).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.maxth5 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(4).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth5 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(4).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
    }

    private double countProbability(double maxp, int priority) {
        return maxp * ((avg - (double) returnMinth(priority)) / ((double) returnMaxth(priority)
                - (double) returnMinth(priority)));
    }

    private long returnMaxth(int priority) {
        if (priority == 1) {
            return this.maxth1;
        } else if (priority == 2) {
            return this.maxth2;
        } else if (priority == 3) {
            return this.maxth3;
        } else if (priority == 4) {
            return this.maxth4;
        } else {
            return this.maxth5;
        }
    }

    private long returnMinth(int priority) {
        if (priority == 1) {
            return this.minth1;
        } else if (priority == 2) {
            return this.minth2;
        } else if (priority == 3) {
            return this.minth3;
        } else if (priority == 4) {
            return this.minth4;
        } else {
            return this.minth5;
        }
    }

    boolean whetherReject(int queueSize, int priority) {
        boolean reject;

        avg = countAverage(queueSize, avg, m);

        if (avg >= returnMinth(priority) && avg < returnMaxth(priority)) {
            count++;

            double pb = countProbability(Double.parseDouble(AccessControlAlgorithm.PROBABILITY_VALUES.
                    get(0).getText()), priority);
            double pa = pb / (1 - count * pb);
            double r = countRandom();

            if (r < pa) {
                reject = true;
                count = 0;
            } else {
                reject = false;
            }
        } else if (avg >= returnMaxth(priority)) {
            reject = true;
            count = 0;
        } else {
            reject = false;
            count = -1;
        }
        return reject;
    }
}
