package algorithms;

import core.Generator;
import core.Switch;

import java.util.Random;

class Auxiliary {
    double countAverage(int queueSize, double avg, double m) {
        double averageTemp;
        double wq = 0.5;

        if (queueSize != 0) {
            averageTemp = (1 - wq) * avg + wq * queueSize;
        } else {
            m = (double) (Switch.IN_ACTION_TIME / Generator.workingTime);
            averageTemp = Math.pow(1 - wq, m) * avg;
        }
        return averageTemp;
    }

    double countRandom() {
        Random random = new Random();

        double rand = (4 + random.nextGaussian()) / 8;

        if (rand < 0) {
            rand = Math.abs(rand);
        }
        return rand;
    }
}
