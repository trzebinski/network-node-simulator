package algorithms;

import core.Request;
import core.Simulation;

class RioAlgorithm extends Auxiliary {
    private int count = -1;
    private double avg;
    private double m;
    private long maxth1;
    private long minth1;
    private long maxth2;
    private long minth2;

    RioAlgorithm() {
        this.avg = 0;
        this.m = 0;
        this.maxth1 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth1 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.maxth2 = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(1).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth2 = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(1).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
    }

    private double countProbability(double maxp, int dsInIp) {
        return maxp * ((avg - (double) returnMinth(dsInIp)) / ((double) returnMaxth(dsInIp)
                - (double) returnMinth(dsInIp)));
    }

    private long returnMaxth(int dsInIp) {
        if (dsInIp == 0) {
            return this.maxth1;
        } else {
            return this.maxth2;
        }
    }

    private long returnMinth(int dsInIp) {
        if (dsInIp == 0) {
            return this.minth1;
        } else {
            return this.minth2;
        }
    }

    boolean whetherReject(int queueSize, int priority) {
        boolean reject;
        int dsInIp = 0;

        if (priority == 5 || priority == 4) {
            dsInIp = 1;
        }

        avg = countAverage(queueSize, avg, m);

        if (avg >= returnMinth(dsInIp) && avg < returnMaxth(dsInIp)) {
            count++;

            double pb = countProbability(Double.parseDouble(AccessControlAlgorithm.PROBABILITY_VALUES.
                    get(0).getText()), dsInIp);
            double pa = pb / (1 - count * pb);
            double r = countRandom();

            if (r < pa) {
                reject = true;
                count = 0;
            } else {
                reject = false;
            }
        } else if (avg >= returnMaxth(dsInIp)) {
            reject = true;
            count = 0;
        } else {
            reject = false;
            count = -1;
        }
        return reject;
    }
}
