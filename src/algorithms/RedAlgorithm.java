package algorithms;

import core.Request;
import core.Simulation;

class RedAlgorithm extends Auxiliary {
    private int count = -1;
    private double avg;
    private double m;
    private long maxth;
    private long minth;

    RedAlgorithm() {
        this.avg = 0;
        this.m = 0;
        this.maxth = (Integer.parseInt(AccessControlAlgorithm.MAXTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
        this.minth = (Integer.parseInt(AccessControlAlgorithm.MINTH_VALUES.get(0).getText())
                * (Simulation.SWITCH_THROUGHPUT / Request.SIZE)) / 100;
    }

    private double countProbability(double maxp) {
        return maxp * ((avg - (double) minth) / ((double) maxth - (double) minth));
    }

    boolean whetherReject(int queueSize) {
        boolean reject;

        avg = countAverage(queueSize, avg, m);

        if (avg >= minth && avg < maxth) {
            count++;

            double pb = countProbability(Double.parseDouble(AccessControlAlgorithm.PROBABILITY_VALUES.
                    get(0).getText()));
            double pa = pb / (1 - count * pb);
            double r = countRandom();

            if (r < pa) {
                reject = true;
                count = 0;
            } else {
                reject = false;
            }
        } else if (avg >= maxth) {
            reject = true;
            count = 0;
        } else {
            reject = false;
            count = -1;
        }
        return reject;
    }
}
