package core;

import gui.GUI;

public class ExecuteRequest implements Runnable {
    private GUI gui;
    private Simulation simulation;

    ExecuteRequest(GUI gui, Simulation simulation) {
        this.gui = gui;
        this.simulation = simulation;
    }

    public GUI getGui() {
        return gui;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }

    @Override
    public void run() {
        try {
            while (Simulation.execute) {
                simulation.getSwitch().executeRequestFromQueue();

                Thread.sleep(RemoveRequest.WAITING_TIME);
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
}
