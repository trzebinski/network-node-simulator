package core;

import gui.GUI;
import jdistlib.generic.GenericDistribution;

import java.util.Random;

public class Generator implements Runnable {
    private GenericDistribution distributionOfTimeIntervalBetweenPackages;
    private GenericDistribution distributionOfTimeIntervalBetweenPackes;
    private GenericDistribution distributionOfPackSize;
    private GUI gui;
    private Simulation simulation;
    private int trafficType;
    private long actualTime;
    public static long workingTime;
    static long generatorsStartedAd;

    Generator(GUI gui, Simulation simulation, int trafficType) {
        this.gui = gui;
        this.simulation = simulation;
        this.trafficType = trafficType;
    }

    private void generateRequest(Random random) {
        actualTime = System.nanoTime();

        Request request = new Request(System.nanoTime(), trafficType, trafficType);
        simulation.getSwitch().addGeneratedRequest(request);
        simulation.getSwitch().addToQueue(request);
    }

    @Override
    public void run() {
        try {
            actualTime = System.nanoTime();

            while (Simulation.execute) {
                int packagesCount = (int) Math.ceil(distributionOfPackSize.random() / 32);

                Random random = new Random();
                for (int i = 0; i < packagesCount; i++) {
                    long timeIntervalBetweenPackages = (long) (100.0 * distributionOfTimeIntervalBetweenPackages.
                            random());
                    Thread.sleep(timeIntervalBetweenPackages);

                    generateRequest(random);
                }

                long timeIntervalBetweenPackes = (long) (100.0 * distributionOfTimeIntervalBetweenPackes.random());
                Thread.sleep(timeIntervalBetweenPackes);
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }

    public GUI getGui() {
        return gui;
    }

    public long getActualTime() {
        return actualTime;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }

    void setDistributionOfTimeIntervalBetweenPackages(GenericDistribution distributionOfTimeIntervalBetweenPackages) {
        this.distributionOfTimeIntervalBetweenPackages = distributionOfTimeIntervalBetweenPackages;
    }

    void setDistributionOfTimeIntervalBetweenPackes(GenericDistribution distributionOfTimeIntervalBetweenPackes) {
        this.distributionOfTimeIntervalBetweenPackes = distributionOfTimeIntervalBetweenPackes;
    }

    void setDistributionOfPackSize(GenericDistribution distributionOfPackSize) {
        this.distributionOfPackSize = distributionOfPackSize;
    }
}
