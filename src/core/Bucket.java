package core;

class Bucket {
    static long THROUGHPUT;
    static long TOKENS;

    static void calculateTokens() {
        long requestSize = Request.SIZE;
        TOKENS = THROUGHPUT / requestSize;
    }
}
