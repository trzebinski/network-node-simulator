package core;

import gui.GUI;

import java.math.BigDecimal;
import java.util.List;

public class RefreshGraph implements Runnable {
    private static long START_TIME;
    private Switch sw;

    RefreshGraph(Switch sw) {
        START_TIME = System.nanoTime();
        this.sw = sw;
    }

    private void updateAccessBarChart(BigDecimal time) {
        int[] statistics = sw.getAccessControlStatistics();

        List<long[]> statisticsByPriority = sw.getAccessControlStatisticsByPriority();

        GUI.ACCESS_BAR_CHART_PANEL.updateStatisticsOfAccessControl(statistics[0], statistics[1]);
        GUI.ACCESS_MULTI_BAR_CHART_PANEL.updateStatisticsOfAccessControl(statisticsByPriority.get(0),
                statisticsByPriority.get(1));
        GUI.ACCESS_LINE_CHART_PANEL.addStatisticsOfAccessControl(statistics[0], statistics[1], time);
    }

    @Override
    public void run() {
        try {
            while (Simulation.execute) {
                BigDecimal a = BigDecimal.valueOf(Simulation.SWITCH_THROUGHPUT - sw.getThroughput());
                BigDecimal b = a.divide(BigDecimal.valueOf(Simulation.SWITCH_THROUGHPUT));

                Thread.sleep(RemoveRequest.WAITING_TIME);

                long time = System.nanoTime() - START_TIME;
                a = BigDecimal.valueOf(time);

                BigDecimal timeBigDecimal = a.divide(BigDecimal.valueOf(1000000000), 3, BigDecimal.ROUND_HALF_UP);
                GUI.graphPanel.updateChart(b.multiply(BigDecimal.valueOf(100)), timeBigDecimal);
                updateAccessBarChart(timeBigDecimal);
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
}
