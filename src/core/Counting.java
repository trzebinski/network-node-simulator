package core;

public class Counting implements Runnable {
    private Switch sw;
    private int kind;

    Counting(Switch sw, int kind) {
        this.sw = sw;
        this.kind = kind;
    }

    @Override
    public void run() {
        try {
            if (kind == 0) {
                Generator.workingTime = System.nanoTime() - Generator.generatorsStartedAd;

                if (sw.getExecutingRequest().size() == 0) {
                    Switch.LAST_EMPTY = System.nanoTime();
                }

                Switch.IN_ACTION_TIME = System.nanoTime() - Switch.LAST_EMPTY;

                Thread.sleep(50);
            } else {
                Thread.sleep(2000);

                for (int i = 0; i < sw.getLastNumberOfGeneratedPackets().size(); i++) {
                    sw.getLastNumberOfGeneratedPackets().set(i, sw.getNumberOfGeneratedPackets().get(i));
                }

                sw.setCanCompare();
                sw.addGeneratedRequest(null);
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
}
