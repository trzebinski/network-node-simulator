package core;

import gui.GUI;

public class RemoveRequest implements Runnable {
    private GUI gui;
    private Simulation simulation;
    static int WAITING_TIME = 25;

    RemoveRequest(GUI gui, Simulation simulation) {
        this.gui = gui;
        this.simulation = simulation;
    }

    public GUI getGui() {
        return gui;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }

    @Override
    public void run() {
        try {
            while (Simulation.execute) {
                simulation.getSwitch().removeRequestFromQueue();

                Thread.sleep(WAITING_TIME);
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
}
