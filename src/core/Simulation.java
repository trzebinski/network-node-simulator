package core;

import gui.GUI;
import jdistlib.generic.GenericDistribution;

import java.util.ArrayList;

public class Simulation {
    private GUI gui;
    private Switch sw;
    private Thread executeRequesterThread;
    private Thread removeRequesterThread;
    private Thread graphRefresherThread;
    private ArrayList<ArrayList<Thread>> generators;
    private String accessControlAlgorithm;
    private String queuingAlgorithm;
    public static final long SWITCH_THROUGHPUT = 1000000L;
    public static int[] classCount = new int[5];
    static boolean execute;

    public Simulation(GUI gui, String queuingAlgorithm, String accessControlAlgorithm) {
        this.gui = gui;
        this.queuingAlgorithm = queuingAlgorithm;
        this.accessControlAlgorithm = accessControlAlgorithm;
        this.generators = new ArrayList<>();
        execute = true;
        sw = new Switch(SWITCH_THROUGHPUT, queuingAlgorithm, accessControlAlgorithm);

        createGenerators();
        Generator.generatorsStartedAd = System.nanoTime();
        startGenerators();
        createQueueManagement();
    }

    public Switch getSwitch() {
        return sw;
    }

    public String getAccessControlAlgorithm() {
        return accessControlAlgorithm;
    }

    public String getQueuingAlgorithm() {
        return queuingAlgorithm;
    }

    public void interuptAll() {
        for (ArrayList<Thread> listOfGeneratorsThreads : generators) {
            for (Thread generatorThread : listOfGeneratorsThreads) {
                generatorThread.interrupt();
            }
        }
        this.executeRequesterThread.interrupt();
        this.removeRequesterThread.interrupt();
        this.graphRefresherThread.interrupt();
    }

    private void createGenerators() {
        for (int i = 0; i < 5; i++) {
            ArrayList<Thread> generatorThread = new ArrayList<>();
            for (int j = 0; j < classCount[i]; j++) {
                Generator generator = new Generator(gui, this, i + 1);
                ArrayList<GenericDistribution> genericDistribution = gui.getChooseDistributionsPanel();
                generator.setDistributionOfPackSize(genericDistribution.get(i * 3));
                generator.setDistributionOfTimeIntervalBetweenPackages(genericDistribution.get(i * 3 + 1));
                generator.setDistributionOfTimeIntervalBetweenPackes(genericDistribution.get(i * 3 + 2));

                Thread thread = new Thread(generator);
                generatorThread.add(thread);
            }
            generators.add(generatorThread);
        }
    }

    private void createQueueManagement() {
        this.executeRequesterThread = new Thread(new ExecuteRequest(gui, this));
        this.executeRequesterThread.start();
        this.removeRequesterThread = new Thread(new RemoveRequest(gui, this));
        this.removeRequesterThread.start();

        new Thread(new Counting(sw, 0)).start();
        if (queuingAlgorithm.equals("SJF"))
            new Thread(new Counting(sw, 1)).start();
        this.graphRefresherThread = new Thread(new RefreshGraph(sw));
        this.graphRefresherThread.start();
    }

    private void startGenerators() {
        for (ArrayList<Thread> listOfGeneratorsThreads : generators) {
            for (Thread generatorThread : listOfGeneratorsThreads) {
                generatorThread.start();
            }
        }
    }
}
