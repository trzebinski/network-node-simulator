package core;

import gui.GUI;

import java.awt.*;

public class Main {
    public static Simulation simulation;

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                new GUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
