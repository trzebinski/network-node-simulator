package core;

import algorithms.AccessControlAlgorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Switch {

    private ArrayList<ArrayList<Request>> queues;
    private ArrayList<HTB> htbAlgorithm;
    private ArrayList<Request> accessAcceptedRequests;
    private ArrayList<Request> accessDeniedRequests;
    private ArrayList<Request> executedRequests;
    private ArrayList<Request> executingRequests;
    private ArrayList<Request> waitingRequests;
    private ArrayList<Integer> numberOfGeneratedPackets;
    private ArrayList<Integer> lastNumberOfGeneratedPackets;
    private AccessControlAlgorithm algorithm;
    private double[] usedQ;
    private double[] maxQ;
    private long[] countOfAllowRequestsByPriority;
    private long[] countOfDenyRequestsByPriority;
    private String queuing;
    private boolean canCompare;
    private int accessedRequests;
    private long throughput;
    public static int PRIORITY_CLASS_COUNT = 5;
    public static long IN_ACTION_TIME = 0;
    static long LAST_EMPTY = 0;

    Switch(long throughput, String queuingAlgorithm, String accessControlAlgorithm) {
        this.throughput = throughput;
        this.queues = new ArrayList<>();
        this.accessDeniedRequests = new ArrayList<>();
        this.accessAcceptedRequests = new ArrayList<>();
        this.executedRequests = new ArrayList<>();
        this.executingRequests = new ArrayList<>();
        this.waitingRequests = new ArrayList<>();

        setNumberOfGeneratedPackets();

        for (int i = 0; i < 5; i++) {
            queues.add(new ArrayList<>());
        }

        this.queuing = queuingAlgorithm;
        this.algorithm = new AccessControlAlgorithm(accessControlAlgorithm);
        setWeightsWFQ();

        this.accessedRequests = 0;

        this.htbAlgorithm = new ArrayList<>();
        Bucket.THROUGHPUT = throughput;
        Bucket.calculateTokens();

        for (int i = 1; i <= 5; i++) {
            this.htbAlgorithm.add(new HTB(i, 0, (15 * Bucket.TOKENS) / 100, (15 * Bucket.TOKENS) / 100
                    + (20 * Bucket.TOKENS) / 100));
        }
        countOfAllowRequestsByPriority = new long[PRIORITY_CLASS_COUNT];
        countOfDenyRequestsByPriority = new long[PRIORITY_CLASS_COUNT];
    }

    private ArrayList<ArrayList<Request>> getQueues() {
        return queues;
    }

    private ArrayList<Request> getWaitingRequest() {
        return waitingRequests;
    }

    private String checkHtbMode(int i) {
        return htbAlgorithm.get(i).checkMode();
    }

    private boolean controlAccess(Request request) {
        boolean isRejected = false;

        if (queuing.equals("FIFO") || queuing.equals("PQ")
                || queuing.equals("SJF") || queuing.equals("WFQ") || queuing.equals("HTB")) {
            isRejected = algorithm.whetherReject(executingRequests.size(), request.getPriority());
            if (isRejected) {
                accessDeniedRequests.add(request);
                countOfDenyRequestsByPriority[request.getPriority() - 1]++;
            } else {
                accessAcceptedRequests.add(request);
                accessedRequests++;
                countOfAllowRequestsByPriority[request.getPriority() - 1]++;
            }
        }
        request.setIsAccepted(!isRejected);
        return isRejected;
    }

    private void borrowTokens() {
        for (int i = 0; i < htbAlgorithm.size(); i++) {
            for (int j = 0; j < htbAlgorithm.size(); j++) {
                if (i != j) {
                    if (htbAlgorithm.get(i).getArWithVariables() < htbAlgorithm.get(i).getCr() && checkHtbMode(j).equals("G") && checkHtbMode(i).equals("Y")) {
                        htbAlgorithm.get(i).borrowThroughput(j, htbAlgorithm.get(j).shareThroughput(htbAlgorithm.get(i).getCr() - htbAlgorithm.get(i).getArWithVariables()));
                    }
                }
            }
        }
    }

    private void deleteShares() {
        for (HTB htb : htbAlgorithm) {
            htb.resetShares();
        }
    }

    private void setNumberOfGeneratedPackets() {
        numberOfGeneratedPackets = new ArrayList<>();
        lastNumberOfGeneratedPackets = new ArrayList<>();
        canCompare = false;

        for (int i = 0; i < 5; i++) {
            numberOfGeneratedPackets.add(0);
            lastNumberOfGeneratedPackets.add(0);
        }
    }

    private void setThroughput(long value) {
        this.throughput = value;
    }

    private void setWeightsWFQ() {
        usedQ = new double[5];
        maxQ = new double[5];

        double a;
        double b = 15;
        double c;

        for (int i = 0; i < usedQ.length; i++) {
            usedQ[i] = 0;

            a = i + 1;
            c = a / b;
            maxQ[i] = c * throughput;
        }
    }

    public synchronized List<Long> getTimeOfAllArrivalPackets() {
        Stream<Request> allReqestStream = Stream.concat(this.accessAcceptedRequests.stream(), this.accessDeniedRequests.stream());
        Stream<Long> arrivalTimesLongStream = allReqestStream.map(Request::getArrivalTime);
        return arrivalTimesLongStream.collect(Collectors.toCollection(ArrayList::new));
    }

    public synchronized List<Long> getTimeOfAllowArrivalPackets() {
        Stream<Long> arrivalTimesLongStream = this.accessAcceptedRequests.stream().map(Request::getArrivalTime);
        return arrivalTimesLongStream.collect(Collectors.toCollection(ArrayList::new));
    }

    public List<List<Long>> getTimeOfAllArrivalPacketsByPriority() {
        Stream<Request> allReqestStream = Stream.concat(this.accessAcceptedRequests.stream(), this.accessDeniedRequests.stream());
        ArrayList<List<Long>> listOfListTimes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            listOfListTimes.add(new ArrayList<>());
        }
        allReqestStream.forEach(r -> listOfListTimes.get(r.getPriority() - 1).add(r.getArrivalTime()));
        return listOfListTimes;
    }

    public List<List<Long>> getTimeOfAllowArrivalPacketsByPriority() {
        Stream<Request> reqestStream = accessAcceptedRequests.stream();
        ArrayList<List<Long>> listOfListTimes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            listOfListTimes.add(new ArrayList<>());
        }
        reqestStream.forEach(r -> listOfListTimes.get(r.getPriority() - 1).add(r.getArrivalTime()));
        return listOfListTimes;
    }

    public ArrayList<Request> getExecutedRequest() {
        return executedRequests;
    }

    public int getAccessedRequests() {
        return accessedRequests;
    }

    public double getUsedQ1() {
        return usedQ[0];
    }

    public double getUsedQ2() {
        return usedQ[1];
    }

    public double getUsedQ3() {
        return usedQ[2];
    }

    public double getUsedQ4() {
        return usedQ[3];
    }

    public double getUsedQ5() {
        return usedQ[4];
    }

    synchronized void addGeneratedRequest(Request request) {
        if (request == null) {
            for (int i = 0; i < numberOfGeneratedPackets.size(); i++) {
                numberOfGeneratedPackets.set(i, 0);
            }
        } else {
            numberOfGeneratedPackets.set(request.getTrafficType() - 1, numberOfGeneratedPackets.get(request.
                    getTrafficType() - 1) + 1);
        }
    }

    synchronized void addToQueue(Request request) {
        if (request == null) {
            throw new IllegalArgumentException("Error! Null requests are not allowed.");
        }
        if (!controlAccess(request)) {
            switch (queuing) {
                case "FIFO":
                    waitingRequests.add(request);
                    break;
                case "SJF":
                    int trafficType = request.getTrafficType();
                    queues.get(trafficType - 1).add(request);
                    break;
                case "PQ":
                    int position = -1;
                    for (int i = 0; i < waitingRequests.size(); i++) {
                        if (request.getPriority() > waitingRequests.get(i).getPriority()) {
                            position = i;
                            break;
                        } else {
                            if (request.getPriority() == waitingRequests.get(i).getPriority()) {
                                if (request.getArrivalTime() < waitingRequests.get(i).getArrivalTime()) {
                                    position = i;
                                    break;
                                }
                            }
                        }
                    }

                    if (position != -1)
                        waitingRequests.add(position, request);
                    else
                        waitingRequests.add(request);
                    break;
                case "WFQ":
                    queues.get(request.getPriority() - 1).add(request);
                    break;
                case "HTB":
                    queues.get(request.getPriority() - 1).add(request);
                    break;
            }
        }
    }

    synchronized void executeRequestFromQueue() {
        switch (queuing) {
            case "FIFO":
            case "PQ":
                while (getWaitingRequest().size() > 0 && getThroughput() - Request.SIZE >= 0) {
                    getExecutingRequest().add(getWaitingRequest().get(0));
                    getWaitingRequest().get(0).setWaitingTime(System.nanoTime());
                    getWaitingRequest().remove(0);
                    setThroughput(getThroughput() - Request.SIZE);
                }
                break;
            case "SJF":
                int count = 0;
                while (getThroughput() - Request.SIZE >= 0 && (getQueues().get(0).size() > 0
                        || getQueues().get(1).size() > 0 || getQueues().get(2).size() > 0
                        || getQueues().get(3).size() > 0 || getQueues().get(4).size() > 0)) {
                    if (canCompare) {
                        int minTraffic = Integer.MAX_VALUE, indication = -1;

                        for (int i = 0; i < 5; i++) {
                            if (lastNumberOfGeneratedPackets.get(i) < minTraffic && getQueues().get(i).size() > 0)
                                indication = i;
                        }
                        if (indication != -1) {
                            getExecutingRequest().add(getQueues().get(indication).get(0));
                            getQueues().get(indication).get(0).setWaitingTime(System.nanoTime());
                            getQueues().get(indication).remove(0);
                            setThroughput(getThroughput() - Request.SIZE);
                        }
                    } else {
                        if (getQueues().get(count).size() > 0) {
                            getExecutingRequest().add(getQueues().get(count).get(0));
                            getQueues().get(count).get(0).setWaitingTime(System.nanoTime());
                            getQueues().get(count).remove(0);
                            setThroughput(getThroughput() - Request.SIZE);
                        }
                        count++;
                        if (count == getQueues().size())
                            count = 0;
                    }
                }
                break;
            case "WFQ":
                while (throughput - Request.SIZE >= 0 &&
                        ((getQueues().get(0).size() > 0 && usedQ[0] + Request.SIZE <= maxQ[0])
                                || (getQueues().get(1).size() > 0 && usedQ[1] + Request.SIZE <= maxQ[1])
                                || (getQueues().get(2).size() > 0 && usedQ[2] + Request.SIZE <= maxQ[2])
                                || (getQueues().get(3).size() > 0 && usedQ[3] + Request.SIZE <= maxQ[3])
                                || (getQueues().get(4).size() > 0 && usedQ[4] + Request.SIZE <= maxQ[4]))) {
                    for (int i = 0; i < 5; i++) {
                        if (getQueues().get(i).size() > 0) {
                            if (usedQ[i] + Request.SIZE <= maxQ[i]) {
                                usedQ[i] += Request.SIZE;
                                executingRequests.add(getQueues().get(i).get(0));
                                getQueues().get(i).get(0).setWaitingTime(System.nanoTime());
                                getQueues().get(i).remove(0);
                                setThroughput(throughput - Request.SIZE);
                            }
                        }
                    }
                }
                break;
            default:
                borrowTokens();
                while (throughput - Request.SIZE >= 0 &&
                        ((getQueues().get(0).size() > 0 && usedQ[0] + Request.SIZE
                                <= htbAlgorithm.get(0).getArWithVariables() * Request.SIZE)
                                || (getQueues().get(1).size() > 0 && usedQ[1] + Request.SIZE
                                <= htbAlgorithm.get(1).getArWithVariables() * Request.SIZE)
                                || (getQueues().get(2).size() > 0 && usedQ[2] + Request.SIZE
                                <= htbAlgorithm.get(2).getArWithVariables() * Request.SIZE)
                                || (getQueues().get(3).size() > 0 && usedQ[3] + Request.SIZE
                                <= htbAlgorithm.get(3).getArWithVariables() * Request.SIZE)
                                || (getQueues().get(4).size() > 0 && usedQ[4] + Request.SIZE
                                <= htbAlgorithm.get(4).getArWithVariables() * Request.SIZE))) {
                    for (int i = 0; i < 5; i++) {
                        if (getQueues().get(i).size() > 0) {
                            if (usedQ[i] + Request.SIZE <= htbAlgorithm.get(i).getArWithVariables() * Request.SIZE) {
                                usedQ[i] += Request.SIZE;
                                executingRequests.add(getQueues().get(i).get(0));
                                getQueues().get(i).get(0).setWaitingTime(System.nanoTime());
                                getQueues().get(i).remove(0);
                                setThroughput(throughput - Request.SIZE);
                                htbAlgorithm.get(i).setR((long) usedQ[i]);
                            }
                        }
                    }
                }
                deleteShares();
                break;
        }
    }

    synchronized void removeRequestFromQueue() {
        Iterator<Request> iterator = getExecutingRequest().iterator();
        while (iterator.hasNext()) {
            Request request = iterator.next();
            if (request.getTimeToEnd() <= 0) {
                iterator.remove();
                executedRequests.add(request);
                if (queuing.equals("WFQ")) {
                    int priority = request.getPriority();
                    usedQ[priority - 1] -= Request.SIZE;
                }
                if (queuing.equals("HTB")) {
                    int priority = request.getPriority();
                    int priorityIndex = priority - 1;
                    usedQ[priorityIndex] -= Request.SIZE;
                    htbAlgorithm.get(priorityIndex).setR((long) usedQ[priorityIndex]);
                }
                setThroughput(getThroughput() + Request.SIZE);
            } else {
                request.setTimeToEnd(request.getTimeToEnd() - RemoveRequest.WAITING_TIME);
            }
        }
    }

    List<long[]> getAccessControlStatisticsByPriority() {
        ArrayList<long[]> list = new ArrayList<>();
        list.add(countOfAllowRequestsByPriority);
        list.add(countOfDenyRequestsByPriority);
        return list;
    }

    ArrayList<Request> getExecutingRequest() {
        return executingRequests;
    }

    ArrayList<Integer> getLastNumberOfGeneratedPackets() {
        return lastNumberOfGeneratedPackets;
    }

    ArrayList<Integer> getNumberOfGeneratedPackets() {
        return numberOfGeneratedPackets;
    }

    int[] getAccessControlStatistics() {
        return new int[]{accessAcceptedRequests.size(), accessDeniedRequests.size()};
    }

    long getThroughput() {
        return throughput;
    }

    void setCanCompare() {
        this.canCompare = true;
    }
}
