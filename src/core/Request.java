package core;

public class Request {
    private static final long EXECUTION_TIME = 100;
    private Boolean isAccepted = null;
    private int dsInIp;
    private int priority;
    private int trafficType;
    private long arrivalTime;
    private long endTime;
    private long timeToEnd;
    private long waitingTime;
    public static final long SIZE = 5000;

    Request(long arrivalTime, int priority, int trafficType) {
        this.dsInIp = (Math.random() < 0.5) ? 0 : 1;
        this.priority = priority;
        this.trafficType = trafficType;
        this.arrivalTime = arrivalTime;
        this.timeToEnd = EXECUTION_TIME;
    }

    public int getDsInIp() {
        return dsInIp;
    }

    public void setDsInIp(int dsInIp) {
        this.dsInIp = dsInIp;
    }

    public int getPriority() {
        return priority;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    int getTrafficType() {
        return trafficType;
    }

    long getArrivalTime() {
        return arrivalTime;
    }

    long getTimeToEnd() {
        return timeToEnd;
    }

    void setTimeToEnd(long timeToEnd) {
        this.timeToEnd = timeToEnd;
    }

    void setWaitingTime(long endTime) {
        this.endTime = endTime;
        waitingTime = endTime - arrivalTime;
    }
}
