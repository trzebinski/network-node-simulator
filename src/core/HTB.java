package core;

import java.util.ArrayList;

class HTB {
    private ArrayList<Integer> borrowedFrom;
    private ArrayList<Long> borrowed;
    private int p;
    private long ar;
    private long cr;
    private long r;
    private long shared;

    HTB(int p, long r, long ar, long cr) {
        this.p = p;
        this.ar = ar;
        this.cr = cr;
        this.r = r;
        this.borrowedFrom = new ArrayList<>();
        this.borrowed = new ArrayList<>();
        this.shared = 0;
    }

    String checkMode() {
        String mode;

        if (r > cr) {
            mode = "R";
        } else if (r >= ar) {
            mode = "Y";
        } else {
            mode = "G";
        }
        return mode;
    }

    long getArWithVariables() {
        long borrow = 0;

        for (Long i : borrowed) {
            borrow += i;
        }
        return ar + borrow - shared;
    }

    long getCr() {
        return this.cr;
    }

    long shareThroughput(long howMuch) {
        if (borrowed.size() > 0) {
            shared = 0;
            return shared;
        } else {
            shared = ar - r;

            if (shared < 0) {
                shared = 0;
            }
            if (shared > cr - ar) {
                shared = cr - ar;
            }
            if (shared > howMuch) {
                shared = howMuch;
            }
            return shared;
        }
    }

    void borrowThroughput(int id, long howMuch) {
        borrowedFrom.add(id);
        borrowed.add(howMuch);
    }

    void resetShares() {
        borrowedFrom = new ArrayList<>();
        borrowed = new ArrayList<>();
        shared = 0;
    }

    void setR(long r) {
        this.r = r;
    }
}
